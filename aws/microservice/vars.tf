# TODO Need to add tags support

variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "service_name" {
  description = "Name of the service. ex myapp"
}

variable "vpc_id" {
  description = "ID of the VPC to use"
}

variable "port" {
  description = "Port application listens on. ex. 8080"
  default = 8080
}

variable "zone_name" {
  description = "Name of the zone to create the DNS record in"
  default = ""
}

#------------------------------------------------------------------------------
# Docker Repository Secret
#------------------------------------------------------------------------------
variable "docker_repo_secret_name" {
  description = "Name of the AWS secret holding credentials to pull docker images"
  default = "dockerhub"
}

#------------------------------------------------------------------------------
# Certificate
#------------------------------------------------------------------------------
variable "certificate_domain" {
  description = "Domain name of the certificate to use"
}

#------------------------------------------------------------------------------
# ALB Target Group
#------------------------------------------------------------------------------
variable "health_check_interval" {
  description = "Interval in seconds health should be checked. ex. 10"
  default = 10
}

variable "health_check_path" {
  description = "URL path the health check should use. ex. /actuator/health"
  default = "/"
}

variable "health_check_timeout" {
  description = "Time in seconds before a health check times out. ex. 5"
  default = 5
}

variable "healthy_threshold" {
  description = "Number of consecutive health checks successes required before considering an unhealthy target healthy. ex. 3"
  default = 3
}

variable "unhealthy_threshold" {
  description = "Number of consecutive health check failures required before considering the target unhealthy. ex. 3"
  default = 3
}

variable "deregistration_delay" {
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused."
  default = 30
}

variable "slow_start" {
  description = "The amount time for targets to warm up before the load balancer sends them a full share of requests. The range is 30-900 seconds or 0 to disable."
  default = 0
}

variable "health_check_http_codes" {
  description = "The HTTP status codes to accept for a health check. ex. 200-299"
  default = "200-400"
}

variable "stickiness_enabled" {
  default = false
}

variable "stickiness_duration" {
  description = "The time period, in seconds, during which requests from a client should be routed to the same target. Default is 86400 (24 hours)"
  default = 86400
}

#------------------------------------------------------------------------------
# Load Balancer
#------------------------------------------------------------------------------

variable "lb_type" {
  description = "Specified the load balancer type. This is either network or application."
  default = "application"
}

variable "lb_allowed_cidr_blocks" {
  description = "The allowed cider blocks that can access the ECS service directly."
  default = []
  type = list(string)
}

# TODO Should be renamed to lb_internal
variable "alb_internal" {
  description = "This should be true for internal load balancers and false for external."
  default = false
}

# TODO Should be renamed to lb_sub net_ids
variable "alb_subnet_ids" {
  description = "List of subnets the load balancer will be placed on"
  type = list(string)
}

variable "lb_idle_timeout" {
  description = "The time in seconds that the connection is allowed to be idle. Only valid for Load Balancers of type application."
  type = number
  default = 60
}

variable "ssl_policy" {
  description = "SSL policy applied to the HTTPS listener. ex. ELBSecurityPolicy-2016-08"
  default = "ELBSecurityPolicy-2016-08"
}

#------------------------------------------------------------------------------
# DNS
#------------------------------------------------------------------------------

variable "create_dns_record" {
  description = "Set to true to create a DNS record, or false if otherwise"
  type = bool
  default = true
}

variable "domain_prefix" {
  description = "DNS record to create"
  default = ""
}

variable "dns_ttl" {
  description = "The TTL value for the DNS record"
  default = "300"
}

#------------------------------------------------------------------------------
# ECS Service
#------------------------------------------------------------------------------

variable "image" {
  description = "The docker image to use. ex. truemark/helloworld-java:latest"
}

variable "cpu" {
  description = "The cpu units allocated to the container. 1024 == 1vCPU"
  default = 1024
}

variable "memory" {
  description = "The amount of memory in MB allocated to the container. ex. 1024"
  default = 2048
}

variable "desired_count" {
  description = "Desired number of task instances"
  default = 2
}

variable "cluster_name" {
  description = "Name of the ECS cluster"
}

variable "ecs_subnet_ids" {
  description = "List of subnets the ecs tasks will be placed on"
  type = list(string)
}

variable "max_capacity" {
  description = "Maximum number of tasks to run"
  default = 4
}

variable "log_retention_days" {
  description = "Number of days logs will be retained in CloudWatch"
  default = 30
}

variable "environment" {
  description = "Environment variables to use in the deask definition"
  default = []
  type = list(map(any))
}

variable "role_policy_vars" {
  default = {}
  type = map(any)
}

variable "assign_public_ip" {
  default = false
  type = bool
}

#------------------------------------------------------------------------------
# Autoscaling Settings
#------------------------------------------------------------------------------

variable "autoscaling_cpu_up_metric_aggregation_type" {
  description = "Valid values are Minimum, Maximum, and Average"
  default = "Average"
}

variable "autoscaling_cpu_up_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start"
  default = 60
}

variable "autoscaling_cpu_up_metric_interval_lower_bound" {
  description = "Difference between the alarm threshold and the CloudWatch metric."
  default = 0
}

variable "autoscaling_cpu_up_scaling_adjustment" {
  description = "The number of members by which to scale, when the adjustment bounds are breached"
  default = 1
}

variable "autoscaling_cpu_down_metric_aggregation_type" {
  description = "Valid values are Minimum, Maximum, and Average"
  default = "Average"
}

variable "autoscaling_cpu_down_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start"
  default = 60
}

variable "autoscaling_cpu_down_metric_interval_upper_bound" {
  description = "Difference between the alarm threshold and the CloudWatch metric."
  default = 0
}

variable "autoscaling_cpu_down_scaling_adjustment" {
  description = "The number of members by which to scale, when the adjustment bounds are breached"
  default = -1
}

variable "alarm_cpu_high_period" {
  description = "The period in seconds over which the specified statistic is applied"
  default = "60"
}

variable "alarm_cpu_high_threshold" {
  description = "The value against which the specified statistic is compared."
  default = "80"
}

variable "alarm_cpu_high_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold."
  default = "1"
}

variable "alarm_cpu_high_statistic" {
  description = "The statistic to apply to the alarm's associated metric. Either of the following is supported: SampleCount, Average, Sum, Minimum, Maximum"
  default = "Average"
}

variable "alarm_cpu_low_period" {
  description = "The period in seconds over which the specified statistic is applied"
  default = "60"
}

variable "alarm_cpu_low_threshold" {
  description = "The value against which the specified statistic is compared."
  default = "10"
}

variable "alarm_cpu_low_evaluation_periods" {
  description = "The number of periods over which data is compared to the specified threshold."
  default = "1"
}

variable "alarm_cpu_low_statistic" {
  description = "The statistic to apply to the alarm's associated metric. Either of the following is supported: SampleCount, Average, Sum, Minimum, Maximum"
  default = "Average"
}
