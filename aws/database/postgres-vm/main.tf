provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/d/subnet.html
data "aws_subnet" "pg" {
  id = var.subnet_id
}

# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "pg" {
  name = "${var.env}-${var.name}-sg"
  vpc_id = data.aws_subnet.pg.vpc_id
  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
resource "aws_security_group_rule" "allow-ssh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.pg.id
}

resource "aws_security_group_rule" "allow-postgresql" {
  type            = "ingress"
  from_port       = 5432
  to_port         = 5432
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.pg.id
}

resource "aws_security_group_rule" "allow-actifio-1" {
  type            = "ingress"
  from_port       = 5106
  to_port         = 5106
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.pg.id
}

resource "aws_security_group_rule" "allow-actifio-2" {
  type            = "ingress"
  from_port       = 56789
  to_port         = 56789
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.pg.id
}

resource "aws_security_group_rule" "egress" {
  type            = "egress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks =   ["0.0.0.0/0"]
  security_group_id = aws_security_group.pg.id
}

# https://www.terraform.io/docs/providers/aws/d/ami.html
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# https://www.terraform.io/docs/providers/aws/r/instance.html
resource "aws_instance" "pg" {
  ami = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.pg.id]
  subnet_id = var.subnet_id

  root_block_device {
    volume_type = "gp2"
    volume_size = var.root_volume_size
    delete_on_termination = true
  }

  # TODO This to be moved to a separate script or salt
  user_data = <<USER_DATA
#!/usr/bin/env bash

# Setup the default user used by pro services
groupadd user
useradd -d /home/user -g user -G sudo,admin -m -s /bin/bash user
mkdir -p /home/user/.ssh
chmod 700 /home/user/.ssh
touch /home/user/.ssh/authorized_keys
chmod 600 /home/user/.ssh/authorized_keys
chown -R user:user /home/user/.ssh

cat <<EOF > /home/user/.ssh/authorized_keys
${var.ssh_authorized_keys}
EOF

echo 'user ALL=(ALL:ALL) NOPASSWD:ALL' > /etc/sudoers.d/user

# Install dependencies
apt-get update && apt-get install -y vim nvme-cli unzip

# Install TrueMark utilities
bash <(curl -sL https://bitbucket.org/truemark/linux-toolbox/raw/master/install)

# Partition and setup

mkswap $(tmdisk 1)
echo "$(blkid | grep $(tmdisk 1) | awk '{print $2}') swap swap defaults 0 0" >> /etc/fstab
swapon $(tmdisk 1)

mkfs.ext4 $(tmdisk 2)
mkdir /var/lib/postgresql
echo "$(blkid | grep $(tmdisk 2) | awk '{print $2}') /var/lib/postgresql ext4 defaults 0 1" >> /etc/fstab
mount -a

cat <<EOF > /root/prep.sh
${var.prep_script}
EOF
chmod +x /root/prep.sh
cd /root && ./prep.sh

USER_DATA

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}

resource "aws_ebs_volume" "swap" {
  availability_zone = data.aws_subnet.pg.availability_zone
  type = "gp2"
  size = var.swap_volume_size

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}
resource "aws_volume_attachment" "swap" {
  device_name = "/dev/xvdb"
  instance_id = aws_instance.pg.id
  volume_id = aws_ebs_volume.swap.id
}

resource "aws_ebs_volume" "db" {
  availability_zone = data.aws_subnet.pg.availability_zone
  type = "gp2"
  size = var.swap_volume_size

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}
resource "aws_volume_attachment" "db" {
  device_name = "/dev/xvdc"
  instance_id = aws_instance.pg.id
  volume_id = aws_ebs_volume.db.id
}

