variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "vpc_id" {
  description = "ID of the VPC to put the target group in"
}

variable "alb_security_group_id" {
  description = "The ID of the security group attached to the ALB"
}

variable "port" {
  description = "Port application listens on. ex. 8080"
  default = 8080
}

variable "name" {
  description = "Name of the service. ex. helloworld"
}

variable "image" {
  description = "The docker image to use. ex. truemark/helloworld-java:latest"
}

variable "cpu" {
  description = "The cpu units allocated to the container. 1024 == 1vCPU"
  default = 1024
}

variable "memory" {
  description = "The amount of memory in MB allocated to the container. ex. 1024"
  default = 2048
}

variable "desired_count" {
  description = "Desired number of task instances"
  default = 2
}

variable "ecs_cluster_id" {
  description = "ID of the ECS cluster to run the service on"
}

variable "ecs_cluster_name" {
  description = "Name of the ECS cluster"
}

variable "subnet_ids" {
  description = "List of subnets the ecs tasks will be placed on"
  type = list(string)
}

variable "alb_target_group_arn" {
  description = "The application load balancer target group arn."
}

variable "max_capacity" {
  description = "Maximum number of tasks to run"
  default = 4
}

variable "log_retention_days" {
  description = "Number of days logs will be retained in CloudWatch"
  default = 30
}

variable "docker_credentials_arn" {
  description = "ARN to the secret for docker Hub"
  default = null
}

variable "environment" {
  description = "Environment variables to use in the deask definition"
  default = []
  type = list(map(any))
}

variable "role_policy_vars" {
  default = {}
  type = map(any)
}
