output "zone_id" {
  value = data.aws_route53_zone.dns.zone_id
}

output "zone_name" {
  value = data.aws_route53_zone.dns.name
}

output "record_ids" {
  value = aws_route53_record.dns.*.id
}

output "record_names" {
  value = aws_route53_record.dns.*.name
}
