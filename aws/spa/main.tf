# See https://bitbucket.org/truemark/terraform-modules/src/master/aws/spa/
provider "aws" {
  version = "~>2.57.0" # https://github.com/terraform-providers/terraform-provider-aws/blob/master/CHANGELOG.md
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/r/s3_bucket.html
resource "aws_s3_bucket" "spa" {
  bucket = "${var.s3_bucket_prefix}-${var.name}-${var.env}"
  acl    = "private"
  force_destroy = var.s3_force_destroy

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
 }

  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/s3_bucket_public_access_block.html
resource "aws_s3_bucket_public_access_block" "spa" {
  bucket = aws_s3_bucket.spa.id

  block_public_acls   = true
  block_public_policy = true
  restrict_public_buckets = true
  ignore_public_acls = true
}

# https://www.terraform.io/docs/providers/aws/r/iam_user.html
resource "aws_iam_user" "spa" {
  name = "${var.name}-${var.env}"
  path = "/terraform/"

  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_access_key.html
resource "aws_iam_access_key" "spa" {
  user = aws_iam_user.spa.name
}

# https://www.terraform.io/docs/providers/aws/r/cloudfront_origin_access_identity.html
resource "aws_cloudfront_origin_access_identity" "spa" {
  comment = "${var.name}-${var.env}"
}

# https://www.terraform.io/docs/providers/aws/d/iam_policy_document.html
data "aws_iam_policy_document" "spa" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.spa.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.spa.iam_arn}"]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["${aws_s3_bucket.spa.arn}"]

    principals {
      type        = "AWS"
      identifiers = ["${aws_cloudfront_origin_access_identity.spa.iam_arn}"]
    }
  }
}

# https://www.terraform.io/docs/providers/aws/r/s3_bucket_policy.html
resource "aws_s3_bucket_policy" "spa" {
  bucket = aws_s3_bucket.spa.id
  policy = data.aws_iam_policy_document.spa.json
}

# https://www.terraform.io/docs/providers/aws/r/acm_certificate.html
resource "aws_acm_certificate" "spa" {
  count = var.certificate_domain == null ? 1 : 0
  domain_name       = var.domain_name
  validation_method = "DNS"

  tags = {
    Environment = var.env
  }

  subject_alternative_names = var.subject_alternative_names

  lifecycle {
    create_before_destroy = true
  }
}

# https://www.terraform.io/docs/providers/aws/d/acm_certificate.html
data "aws_acm_certificate" "spa" {
  count = var.certificate_domain != null ? 1 : 0
  domain = var.certificate_domain
  most_recent = true
}

# https://www.terraform.io/docs/providers/aws/d/route53_zone.html
data "aws_route53_zone" "spa" {
  count         = var.zone_name == null ? 0 : 1
  name          = var.zone_name
  private_zone  = false
}

# https://www.terraform.io/docs/providers/aws/r/route53_record.html
resource "aws_route53_record" "validation" {
  count = var.certificate_domain == null && var.zone_name != null ? (1 + length(var.subject_alternative_names)) : 0
  zone_id = data.aws_route53_zone.spa[min(0, count.index)].zone_id
  name    = aws_acm_certificate.spa[min(0, count.index)].domain_validation_options[count.index].resource_record_name
  type    = aws_acm_certificate.spa[min(0, count.index)].domain_validation_options[count.index].resource_record_type
  ttl     = "30"
  records = [aws_acm_certificate.spa[min(0, count.index)].domain_validation_options[count.index].resource_record_value]
}

# https://www.terraform.io/docs/providers/aws/r/acm_certificate_validation.html
resource "aws_acm_certificate_validation" "spa" {
  count = var.certificate_domain == null ? 1 : 0
  certificate_arn         = aws_acm_certificate.spa[count.index].arn
  validation_record_fqdns = aws_acm_certificate.spa[count.index].domain_validation_options.*.resource_record_name
}

# https://www.terraform.io/docs/providers/aws/r/iam_role.html
resource "aws_iam_role" "redirect" {
  count = var.enable_redirect && length(var.subject_alternative_names) > 0 ? 1 : 0
  name = "${var.name}-${var.env}-lambda-redirect"
  assume_role_policy = file("lambda_redirect_policy.json")
  tags = {
    Name = "${var.name}-${var.env}-lambda-redirect"
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/template/d/file.html
data "template_file" "redirect" {
  template = file("lambda_redirect.js")
  vars = {
    domain = var.domain_name
  }
}

# https://www.terraform.io/docs/providers/local/r/file.html
resource "local_file" "redirect" {
  content     = data.template_file.redirect.rendered
  filename    = "redirect.js"
  depends_on = [data.template_file.redirect]
}

# https://www.terraform.io/docs/providers/archive/d/archive_file.html
data "archive_file" "redirect" {
  type        = "zip"
  source_file = local_file.redirect.filename
  output_path = "redirect.zip"
  depends_on = [local_file.redirect]
}

# https://www.terraform.io/docs/providers/aws/r/lambda_function.html
resource "aws_lambda_function" "redirect" {
  count         = var.enable_redirect && length(var.subject_alternative_names) > 0 ? 1 : 0
  filename      = "redirect.zip"
  function_name = "${var.name}-${var.env}-lambda-redirect"
  role          = aws_iam_role.redirect[count.index].arn
  handler       = "redirect.handler"
  publish       = true
  runtime       = "nodejs12.x"
  tags = {
    Environment = var.env
  }
}

locals {
  s3_origin_id = "S3-${var.name}-${var.env}"
}

# https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html
resource "aws_cloudfront_distribution" "spa" {

  origin {
    domain_name = aws_s3_bucket.spa.bucket_regional_domain_name
    origin_id   = local.s3_origin_id
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.spa.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "${var.name} ${var.env}"
  default_root_object = "index.html"
  aliases = concat(list(var.domain_name), var.subject_alternative_names)

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = local.s3_origin_id

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }

    dynamic "lambda_function_association" {
      for_each = aws_lambda_function.redirect
      content {
          event_type = "viewer-request"
          lambda_arn = lambda_function_association.value.qualified_arn
      }
    }

    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  price_class = var.price_class

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = {
    Environment = var.env
  }

  viewer_certificate {
    minimum_protocol_version = var.minimum_protocol_version
    ssl_support_method = "sni-only"
    acm_certificate_arn = var.certificate_domain != null ? element(data.aws_acm_certificate.spa.*.arn, 0) : element(aws_acm_certificate.spa.*.arn, 0)
  }

  custom_error_response {
    error_code = "404"
    response_page_path = "/index.html"
    response_code = "200"
    error_caching_min_ttl = "300"
  }

  custom_error_response {
    error_code = "403"
    response_page_path = "/index.html"
    response_code = "200"
    error_caching_min_ttl = "300"
  }
}

# https://www.terraform.io/docs/providers/aws/r/route53_record.html
resource "aws_route53_record" "spa" {
  count = var.zone_name == null ? 0 : 1
  zone_id = data.aws_route53_zone.spa[count.index].zone_id
  name    = var.domain_name
  type    = "A"
  alias {
    name = aws_cloudfront_distribution.spa.domain_name
    zone_id = aws_cloudfront_distribution.spa.hosted_zone_id
    evaluate_target_health = false
  }
}

data "aws_route53_zone" "sans" {
  count         = length(var.subject_alternative_name_zone_names)
  name          = var.subject_alternative_name_zone_names[count.index]
  private_zone  = false
}

resource "aws_route53_record" "sans" {
  count = length(var.subject_alternative_name_zone_names) != length(var.subject_alternative_names) ? 0 : length(var.subject_alternative_names)
  zone_id = data.aws_route53_zone.sans[count.index].zone_id
  name    = var.subject_alternative_names[count.index]
  type    = "A"
  alias {
    name = aws_cloudfront_distribution.spa.domain_name
    zone_id = aws_cloudfront_distribution.spa.hosted_zone_id
    evaluate_target_health = false
  }
}

#https://www.terraform.io/docs/providers/aws/r/iam_user_policy.html
resource "aws_iam_user_policy" "spa" {
  name = "${var.name}-${var.env}"
  user = aws_iam_user.spa.name

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "SyncAction1",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:GetObjectAcl",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject",
                "s3:GetBucketLocation"
            ],
            "Resource": [
                "${aws_s3_bucket.spa.arn}",
                "${aws_s3_bucket.spa.arn}/*"
            ]
        },
        {
            "Sid": "SyncAction3",
            "Effect": "Allow",
            "Action": "s3:ListBucket",
            "Resource": "${aws_s3_bucket.spa.arn}"
        },
        {
            "Sid": "Invalidate",
            "Effect": "Allow",
            "Action": "cloudfront:CreateInvalidation",
            "Resource": "${aws_cloudfront_distribution.spa.arn}"
        }
    ]
}
EOF
}
