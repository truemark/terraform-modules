provider "aws" {
  version = "~>2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/r/ecs_cluster.html
resource "aws_ecs_cluster" "main" {
  name = "${var.name}-${var.env}${var.suffix}"
  tags = {
    Environment = var.env
    Name = "${var.name}-${var.env}${var.suffix}"
  }
}

