variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "name" {
  description = "The name of the EC2 instance. This will be prefixed with {env}-"
}

variable "subnet_id" {
  description = "The ID subnet name to create the EC2 instance in."
}

variable "allowed_cidr_blocks" {
  description = "Allowed CIDR blocks that can SSh to this host"
  type = list(string)
  default = ["0.0.0.0/0"]
}

variable "instance_type" {
  description = "Instance type to create. ex. t3.2xlarge"
  default = "t3.2xlarge"
}

variable "key_name" {
  description = "The SSH key to use"
}

variable "root_volume_size" {
  description = "The size in GB of the root volume. ex 30"
  default = 50
}

variable "swap_volume_size" {
  description = "The size in GB of the swap volume. ex 32"
  default = 32
}

variable "software_volume_size" {
  description = "The size in GB of the software volume (/u01). ex 120"
  default = 120
}

variable "data_volume_size" {
  description = "The size in GB of the data volume (/u02). ex 10"
  default = 10
}

variable "fra_volume_size" {
  description = "The size in GB of the logs volume (/u03). ex 10"
  default = 11
}

variable "ssh_authorized_keys" {
  description = "SSH keys to add to /home/user/.ssh/authorized_keys"
}

variable "prep_script" {
  description = "A shell script holding any additional preparation steps to be run"
}
