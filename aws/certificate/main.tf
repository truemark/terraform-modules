provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/r/acm_certificate.html
resource "aws_acm_certificate" "crt" {
  domain_name       = var.domain_name
  validation_method = "DNS"

  tags = {
    Environment = var.env
  }

  lifecycle {
    create_before_destroy = true
  }
}

# https://www.terraform.io/docs/providers/aws/d/route53_zone.html
data "aws_route53_zone" "crt" {
  name         = var.zone_name
  private_zone = false
}

# https://www.terraform.io/docs/providers/aws/r/route53_record.html
resource "aws_route53_record" "validation" {
  zone_id = data.aws_route53_zone.crt.zone_id
  name    = aws_acm_certificate.crt.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.crt.domain_validation_options[0].resource_record_type
  ttl     = "30"
  records = [aws_acm_certificate.crt.domain_validation_options[0].resource_record_value]
}

# https://www.terraform.io/docs/providers/aws/r/acm_certificate_validation.html
resource "aws_acm_certificate_validation" "crt" {
  certificate_arn         = "${aws_acm_certificate.crt.arn}"
  validation_record_fqdns = ["${aws_route53_record.validation.fqdn}"]
}
