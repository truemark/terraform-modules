output "ecr_repository_ids" {
  value = aws_ecr_repository.ecr[*].id
}

output "ecr_repository_names" {
  value = aws_ecr_repository.ecr[*].name
}

output "ecr_repository_registry_ids" {
  value = aws_ecr_repository.ecr[*].registry_id
}

output "ecr_repository_urls" {
  value = aws_ecr_repository.ecr[*].repository_url
}
