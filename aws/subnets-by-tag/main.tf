# See https://bitbucket.org/truemark/terraform-modules/src/master/aws/zone/
provider "aws" {
  version = "~>2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

data "aws_subnet_ids" "main" {
  vpc_id = var.vpc_id
  filter {
    name   = "tag:${var.tag_name}"
    values = var.tag_values
  }
}
