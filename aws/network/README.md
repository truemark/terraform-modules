# Network Module

This module will setup the following resources
 * VPC using the provided CIDR block
 * Default network ACL allowing all ingress and egress
 * Default routing table
 * Subnet(s) for NAT gateways for each availability zone provided 
 * Internet Gateway
 * Nat Gateway(s) for each availability zone provided
 * Routing table for each subnet


Here is an example 

```hcl-terraform
terraform {
  source = "git::git@bitbucket.org:truemark/terraform-modules.git//aws/network?ref=master"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  region              = "us-east-1"
  cidr_block          = "10.0.0.0/16"
  availability_zones  = ["a", "b", "c"]
}
```