provider "aws" {
  version = "~>2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

resource "aws_ecr_repository" "ecr" {
  count = length(var.repositories)
  name                 = var.repositories[count.index].name
  image_tag_mutability = var.repositories[count.index].image_tag_mutability

  image_scanning_configuration {
    scan_on_push = var.repositories[count.index].scan_on_push
  }
}
