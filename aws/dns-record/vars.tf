variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "zone_name" {
  description = "The name of the zone to put the DNS record in"
}

variable "zone_private" {
  description = "True if the zone is private, false if otherwise."
  default = false
}

variable "records" {
  description = "The DNS records to create"
  type = list(object({
    name = string
    type = string
    values = list(string)
    ttl = number
  }))
}
