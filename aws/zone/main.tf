# See https://bitbucket.org/truemark/terraform-modules/src/master/aws/zone/
provider "aws" {
  version = "~>2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

resource "aws_route53_zone" "zone" {
  count = length(var.zone_names)
  name = var.zone_names[count.index]

  tags = {
    Environment = var.env
  }
}
