provider "azurerm" {
  version = "~>1.38.0"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "azurerm" {}
}
