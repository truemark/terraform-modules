output "vpc_id" {
  value = aws_vpc.main.id
}

output "vpc_arn" {
  value = aws_vpc.main.arn
}

output "subnet_ids" {
  value = aws_subnet.main[*].id
}

output "network_acl_ids" {
  value = aws_default_network_acl.main[*].id
}

output "default_route_table_id" {
  value = aws_default_route_table.main.id
}

output "internet_gateway_id" {
  value =  aws_internet_gateway.main.id
}

output "nat_gateway_eip_ids" {
  value = aws_eip.main[*].id
}

output "nat_gateway_ids" {
  value = aws_nat_gateway.main[*].id
}

output "nat_route_table_ids" {
  value = aws_route_table.nat[*].id
}

output "nat_subnet_ids" {
  value = aws_subnet.nat[*].id
}

