provider "azurerm" {
  version = "~>1.38.0"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "azurerm" {}
}

# https://www.terraform.io/docs/providers/azurerm/r/resource_group.html
resource "azurerm_resource_group" "azsql" {
  count    = var.rg_create == true ? 1 : 0
  name     = var.rg_name
  location = var.location
  tags = {
    environment = var.env
  }
}

# https://www.terraform.io/docs/providers/azurerm/d/resource_group.html
data "azurerm_resource_group" "azsql" {
  count   = var.rg_create == false ? 1 : 0
  name    = var.rg_name
}

# https://www.terraform.io/docs/providers/azurerm/r/sql_server.html
resource "azurerm_sql_server" "azsql" {
  name                         = var.sql_server_name
  resource_group_name          = var.rg_create == true ? azurerm_resource_group.azsql[0].name : data.azurerm_resource_group.azsql[0].name
  location                     = var.rg_create == true ? azurerm_resource_group.azsql[0].location : data.azurerm_resource_group.azsql[0].location
  version                      = var.sql_server_version
  administrator_login          = var.administrator_login
  administrator_login_password = var.administrator_login_password

  tags = {
    environment = var.env
  }
}

# https://www.terraform.io/docs/providers/azurerm/r/mssql_elasticpool.html
resource "azurerm_mssql_elasticpool" "azsql" {
  count               = length(var.elastic_pools_dtu)
  name                = var.elastic_pools_dtu[count.index].name
  resource_group_name = var.rg_create == true ? azurerm_resource_group.azsql[0].name : data.azurerm_resource_group.azsql[0].name
  location            = var.rg_create == true ? azurerm_resource_group.azsql[0].location : data.azurerm_resource_group.azsql[0].location
  server_name         = azurerm_sql_server.azsql.name
  max_size_gb         = var.elastic_pools_dtu[count.index].max_size_gb

  sku {
    name     = var.elastic_pools_dtu[count.index].sku.name
    tier     = var.elastic_pools_dtu[count.index].sku.tier
    capacity = var.elastic_pools_dtu[count.index].sku.capacity
  }

  per_database_settings {
    min_capacity = var.elastic_pools_dtu[count.index].db_min_capacity
    max_capacity = var.elastic_pools_dtu[count.index].db_max_capacity
  }

  tags = {
    environment = var.env
  }
}

resource "azurerm_sql_firewall_rule" "azsql" {
  count               = length(var.firewall_rules)
  name                = var.firewall_rules[count.index].name
  resource_group_name = var.rg_create == true ? azurerm_resource_group.azsql[0].name : data.azurerm_resource_group.azsql[0].name
  server_name         = azurerm_sql_server.azsql.name
  start_ip_address    = var.firewall_rules[count.index].start_ip_address
  end_ip_address      = var.firewall_rules[count.index].end_ip_address
}
