output "security_group_id" {
  value = aws_security_group.pg.id
}

output "instance_id" {
  value = aws_instance.pg.id
}

output "swap_ebs_volume_id" {
  value = aws_ebs_volume.swap.id
}

output "swap_volume_attachment_id" {
  value = aws_volume_attachment.swap.id
}

output "db_ebs_volume_id" {
  value = aws_ebs_volume.db.id
}

output "db_volume_attachment_id" {
  value = aws_volume_attachment.db.id
}
