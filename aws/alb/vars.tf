variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "vpc_id" {
  description = "ID of the VPC to use"
}

variable "name" {
  description = "Name of the load balancer. This will be prefixed with the environment.. ex. myapp"
}

variable "internal" {
  description = "This should be true for internal load balancers and false for external."
}

variable "subnet_ids" {
  description = "List of subnets the load balancer will be placed on"
  type = list(string)
}

variable "default_target_group_arn" {
  description = "The default ALB target for the load balancer if no rules match."
}

variable "ssl_policy" {
  description = "SSL policy applied to the HTTPS listener. ex. ELBSecurityPolicy-2016-08"
  default = "ELBSecurityPolicy-2016-08"
}

variable "certificate_domain" {
  description = "Domain name of the certificate to use"
}

variable "create_dns_record" {
  description = "Set to true to create a DNS record, or false if otherwise"
  default = true
}

variable "zone_name" {
  description = "Name of the zone to create the DNS record in"
  default = ""
}

variable "domain_name" {
  description = "DNS record to create"
  default = ""
}

variable "dns_ttl" {
  description = "The TTL value for the DNS record"
  default = "300"
}
