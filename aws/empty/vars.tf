variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}
