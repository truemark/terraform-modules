variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "tag_name" {
  description = "Name of the tag to match"
}

variable "tag_values" {
  description = "Values of the tag to match"
  type = list(string)
}


