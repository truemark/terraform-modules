output "s3_bucket_id" {
  value = aws_s3_bucket.spa.id
}

output "s3_bucket_public_access_block_id" {
  value = aws_s3_bucket_public_access_block.spa.id
}

output "iam_user_id" {
  value = aws_iam_user.spa.id
}

output "iam_access_key_id" {
  value = aws_iam_access_key.spa.id
}

output "iam_access_key_secret" {
  value = aws_iam_access_key.spa.secret
}

output "iam_access_key_encrypted_secret" {
  value = aws_iam_access_key.spa.encrypted_secret
}

output "cloudfront_origin_access_identity_id" {
  value = aws_cloudfront_origin_access_identity.spa.id
}

output "s3_bucket_policy_id" {
  value = aws_s3_bucket_policy.spa.id
}

output "acm_certificate_id" {
  value = aws_acm_certificate.spa.*.id
}

output "route53_validation_record_ids" {
  value = aws_route53_record.validation.*.id
  //value = (var.zone_name == null || var.certificate_domain == null) ? null : aws_route53_record.validation[length(aws_route53_record.validation)].id
//  value = var.zone_name == null && length(aws_route53_record.validation.*) > 0 ? null : aws_route53_record.validation[min(0, 0)].id
}

output "acm_certificate_validation_id" {
  value = aws_acm_certificate_validation.spa.*.id
}

output "cloudfront_distribution_id" {
  value = aws_cloudfront_distribution.spa.id
}

output "domain_name_route53_record_id" {
  value = join("", aws_route53_record.spa.*.id)
}

output "subject_alternative_names_route53_record_ids" {
  value = join("", aws_route53_record.sans.*.id)
}

output "iam_user_policy_id" {
  value = aws_iam_user_policy.spa.id
}
