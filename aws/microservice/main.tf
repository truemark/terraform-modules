provider "aws" {
  version = "=2.68.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

locals {
  env = var.env
}

#------------------------------------------------------------------------------
# Certificate
#------------------------------------------------------------------------------
# https://www.terraform.io/docs/providers/aws/d/acm_certificate.html
data "aws_acm_certificate" "app" {
  domain = var.certificate_domain
  statuses = ["ISSUED"]
  most_recent = true
}

#------------------------------------------------------------------------------
# Load Balancer Target Group
#------------------------------------------------------------------------------

# https://www.terraform.io/docs/providers/aws/r/lb_target_group.html
resource "aws_lb_target_group" "app" {
  name     = var.service_name
  port     = var.port
  protocol = var.lb_type == "application" ? "HTTP" : "TCP"
  vpc_id   = var.vpc_id
  target_type = "ip"
  deregistration_delay = var.deregistration_delay
  slow_start = var.slow_start

  dynamic "health_check" {
    for_each = var.lb_type == "application" ? [1] : []
    content {
      enabled = true
      interval = var.health_check_interval
      path = var.health_check_path
      timeout = var.health_check_timeout
      healthy_threshold = var.healthy_threshold
      unhealthy_threshold = var.unhealthy_threshold
      matcher = var.health_check_http_codes
    }
  }

  dynamic "health_check" {
    for_each = var.lb_type == "network" ? [1] : []
    content {
      enabled = true
      path = var.health_check_path
      interval = var.health_check_interval
      healthy_threshold = var.healthy_threshold
      unhealthy_threshold = var.unhealthy_threshold
    }
  }

  dynamic "stickiness" {
    for_each = var.lb_type == "application" ? [1] : []
    content {
      type = "lb_cookie"
      enabled = var.stickiness_enabled
      cookie_duration = var.stickiness_duration
    }
  }
}

#------------------------------------------------------------------------------
# ALB
#------------------------------------------------------------------------------
# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "alb" {
  name = "${var.service_name}-alb"
  description = "Controls access to ALB ${var.service_name}"
  vpc_id = var.vpc_id
  tags = {
    Name = "${var.service_name}-alb"
    Environment = "main"
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
resource "aws_security_group_rule" "alb_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "alb_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "alb_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.alb.id
}

# https://www.terraform.io/docs/providers/aws/r/lb.html
resource "aws_alb" "app" {
  name                = var.service_name
  load_balancer_type  = var.lb_type
  internal        = var.alb_internal
  subnets         = var.alb_subnet_ids
  security_groups = var.lb_type == "application" ? [aws_security_group.alb.id] : []
  idle_timeout = var.lb_idle_timeout
}

# https://www.terraform.io/docs/providers/aws/r/lb_listener.html
resource "aws_lb_listener" "redirect" {
  load_balancer_arn = aws_alb.app.arn
  port              = "80"
  protocol          = var.lb_type == "application" ? "HTTP" : "TCP"

  dynamic "default_action" {
    for_each = var.lb_type == "application" ? [1] : []
    content {
      type = "redirect"
      redirect {
        port        = "443"
        protocol    = "HTTPS"
        status_code = "HTTP_301"
      }
    }
  }

  dynamic "default_action" {
    for_each = var.lb_type == "network" ? [1] : []
    content {
      type = "forward"
      target_group_arn = aws_lb_target_group.app.arn
    }
  }
}

# https://www.terraform.io/docs/providers/aws/r/lb_listener.html
resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_alb.app.arn
  port              = "443"
  protocol          = var.lb_type == "application" ? "HTTPS" : "TLS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = data.aws_acm_certificate.app.arn

  default_action {
    target_group_arn = aws_lb_target_group.app.arn
    type = "forward"
  }
}

#------------------------------------------------------------------------------
# DNS
#------------------------------------------------------------------------------
# https://www.terraform.io/docs/providers/aws/d/route53_zone.html
data "aws_route53_zone" "app" {
  count = var.create_dns_record ? 1 : 0
  name = var.zone_name
}

# https://www.terraform.io/docs/providers/aws/r/route53_record.html
resource "aws_route53_record" "app" {
  count   = var.create_dns_record ? 1 : 0
  zone_id = data.aws_route53_zone.app[count.index].id
  name    = "${var.domain_prefix}.${var.zone_name}"
  type    = "A"
  alias {
    name = aws_alb.app.dns_name
    zone_id = aws_alb.app.zone_id
    evaluate_target_health = false
  }
}

#------------------------------------------------------------------------------
# ECS Service
#------------------------------------------------------------------------------
resource "aws_cloudwatch_log_group" "ecs" {
  name = var.service_name
  retention_in_days = 30

  tags = {
    Name = var.service_name
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "ecs_tasks" {
  name        = "${var.service_name}-task"
  description = "allow inbound access from the ALB only"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.service_name}-task"
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
# This rule has no affect on network load balancers
resource "aws_security_group_rule" "ecs_tasks_alb" {
  type              = "ingress"
  from_port         = var.port
  to_port           = var.port
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_tasks.id
  source_security_group_id = aws_security_group.alb.id
}

resource "aws_security_group_rule" "ecs_tasks_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_tasks.id
}

resource "aws_security_group_rule" "ecs_tasks_ingress" {
  count = length(var.lb_allowed_cidr_blocks) == 0 ? 0 : 1
  type              = "ingress"
  from_port         = var.port
  to_port           = var.port
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_tasks.id
  cidr_blocks = var.lb_allowed_cidr_blocks
}

# https://www.terraform.io/docs/providers/aws/r/iam_role.html
resource "aws_iam_role" "ecs" {
  name = "${var.service_name}-ecs"
  assume_role_policy = file("assume_role_policy.json")
  tags = {
    Name = var.service_name
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/template/d/file.html
data "template_file" "ecs_iam_role_policy" {
  template = file("ecs_role_policy.json")
  vars = {
    environment = jsonencode(var.environment)
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_role_policy.html
resource "aws_iam_role_policy" "ecs" {
  name = var.service_name
  role = aws_iam_role.ecs.id
  policy = data.template_file.ecs_iam_role_policy.rendered
}


# https://www.terraform.io/docs/providers/aws/r/iam_role.html
resource "aws_iam_role" "task" {
  name = "${var.service_name}-task"
  assume_role_policy = file("assume_role_policy.json")
  tags = {
    Name = var.service_name
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_role_policy.html
resource "aws_iam_role_policy" "task" {
  name = "${var.service_name}-task"
  role = aws_iam_role.task.id
  policy = file("task_role_policy.json")
}


# https://www.terraform.io/docs/providers/template/d/file.html
data "template_file" "task_definition" {
  # https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html
  template = file("task_definition.json")
  vars = {
    name      = var.service_name
    image     = var.image
    port      = var.port
    cpu       = var.cpu
    memory    = var.memory
    region    = var.region
    log_group = aws_cloudwatch_log_group.ecs.name
    log_stream_prefix = var.service_name
    environment = jsonencode(var.environment)
  }
}

# https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html
resource "aws_ecs_task_definition" "service" {
  container_definitions = data.template_file.task_definition.rendered
  family = var.service_name
  execution_role_arn = aws_iam_role.ecs.arn
  task_role_arn = aws_iam_role.task.arn
  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu = var.cpu
  memory = var.memory
}


data "aws_ecs_cluster" "app" {
  cluster_name = var.cluster_name
}

# https://www.terraform.io/docs/providers/aws/r/ecs_service.html
resource "aws_ecs_service" "service" {
  name = var.service_name
  cluster = data.aws_ecs_cluster.app.id
  task_definition = aws_ecs_task_definition.service.arn
  desired_count = var.desired_count
  launch_type = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = var.ecs_subnet_ids
    assign_public_ip = var.assign_public_ip
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.app.arn
    container_name   = var.service_name
    container_port   = var.port
  }

  depends_on = [
    aws_iam_role_policy.ecs,
    aws_iam_role_policy.task,
    aws_ecs_task_definition.service,
    aws_alb.app,
    aws_lb_listener.app,
    aws_lb_target_group.app,
    aws_security_group.ecs_tasks]

  # Allow external changes to desired_count
  lifecycle {
    ignore_changes = [desired_count]
  }
}

# https://www.terraform.io/docs/providers/aws/r/appautoscaling_target.html
resource "aws_appautoscaling_target" "target" {
  service_namespace  = "ecs"
  resource_id        = "service/${var.cluster_name}/${var.service_name}"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity       = var.desired_count
  max_capacity       = var.max_capacity

  depends_on = [aws_ecs_service.service]
}

# Automatically scale capacity up by one
resource "aws_appautoscaling_policy" "up" {
  name               = "${var.service_name}-up"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.target.resource_id
  service_namespace  = aws_appautoscaling_target.target.service_namespace
  scalable_dimension = aws_appautoscaling_target.target.scalable_dimension

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.autoscaling_cpu_up_cooldown
    metric_aggregation_type = var.autoscaling_cpu_up_metric_aggregation_type

    step_adjustment {
      metric_interval_lower_bound = var.autoscaling_cpu_up_metric_interval_lower_bound
      scaling_adjustment          = var.autoscaling_cpu_up_scaling_adjustment
    }
  }

  depends_on = [aws_appautoscaling_target.target]
}

# Automatically scale capacity down by one
resource "aws_appautoscaling_policy" "down" {
  name               = "${var.service_name}-down"
  policy_type        = "StepScaling"
  resource_id        = aws_appautoscaling_target.target.resource_id
  scalable_dimension = aws_appautoscaling_target.target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.target.service_namespace

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = var.autoscaling_cpu_down_cooldown
    metric_aggregation_type = var.autoscaling_cpu_down_metric_aggregation_type

    step_adjustment {
      metric_interval_upper_bound = var.autoscaling_cpu_down_metric_interval_upper_bound
      scaling_adjustment          = var.autoscaling_cpu_down_scaling_adjustment
    }
  }

  depends_on = [aws_appautoscaling_target.target]
}

# CloudWatch alarm that triggers the autoscaling up policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_high" {
  alarm_name          = "${var.service_name}_cpu_utilization_high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.alarm_cpu_high_evaluation_periods
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = var.alarm_cpu_high_period
  statistic           = var.alarm_cpu_high_statistic
  threshold           = var.alarm_cpu_high_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = var.service_name
  }

  alarm_actions = [aws_appautoscaling_policy.up.arn]
}

# CloudWatch alarm that triggers the autoscaling down policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_low" {
  alarm_name          = "${var.service_name}_cpu_utilization_low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = var.alarm_cpu_low_evaluation_periods
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = var.alarm_cpu_low_period
  statistic           = var.alarm_cpu_low_statistic
  threshold           = var.alarm_cpu_low_threshold

  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = var.service_name
  }

  alarm_actions = [aws_appautoscaling_policy.down.arn]
}
