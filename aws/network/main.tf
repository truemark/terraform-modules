# See https://bitbucket.org/truemark/terraform-modules/src/master/aws/network
provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/d/vpc.html
resource "aws_vpc" "main" {
  cidr_block = var.cidr_block
  instance_tenancy = "default"
  tags = {
    Environment = var.env
    Name = "${var.env}-vpc"
  }
}

# https://www.terraform.io/docs/providers/aws/r/subnet.html
resource "aws_subnet" "main" {
  count = length(var.availability_zones)
  vpc_id = aws_vpc.main.id
  cidr_block = cidrsubnet(var.cidr_block, var.newbits, count.index)
  availability_zone = "${var.region}${var.availability_zones[count.index]}"
  tags = {
    Environment = var.env
    Classification = "public"
    Name = "${var.env}-${var.availability_zones[count.index]}"
  }
}

# https://www.terraform.io/docs/providers/aws/r/subnet.html
resource "aws_subnet" "nat" {
  count = var.skip_nat ? 0 : length(var.availability_zones)
  vpc_id = aws_vpc.main.id
  cidr_block = cidrsubnet(var.cidr_block, var.nat_newbits, count.index + var.nat_offset)
  availability_zone = "${var.region}${var.availability_zones[count.index]}"
  tags = {
    Environment = var.env
    Classification = "private"
    Name = "${var.env}-nat-${var.availability_zones[count.index]}"
  }
}

# https://www.terraform.io/docs/providers/aws/r/default_network_acl.html
resource "aws_default_network_acl" "main" {
  default_network_acl_id = aws_vpc.main.default_network_acl_id
  tags = {
    Environment = var.env
    Name = "${var.env}-acl"
  }
  subnet_ids = concat(aws_subnet.main.*.id, aws_subnet.nat.*.id)

  # Allow external changes to subnet_ids
  lifecycle {
    ignore_changes = [subnet_ids, ingress, egress]
  }
}

# https://www.terraform.io/docs/providers/aws/r/network_acl_rule.html
resource "aws_network_acl_rule" "main-egress" {
  network_acl_id = aws_default_network_acl.main.id
  rule_number    = 200
  egress         = true
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}

resource "aws_network_acl_rule" "main-ingress" {
  network_acl_id = aws_default_network_acl.main.id
  rule_number    = 200
  egress         = false
  protocol       = "-1"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
}

# https://www.terraform.io/docs/providers/aws/r/default_route_table.html
resource "aws_default_route_table" "main" {
  default_route_table_id = aws_vpc.main.default_route_table_id
  tags = {
    Environment = var.env
    Name = "${var.env}-rtb"
  }
}

# https://www.terraform.io/docs/providers/aws/r/internet_gateway.html
resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags = {
    Environment = var.env
    Name = "${var.env}-igw"
  }
}

# https://www.terraform.io/docs/providers/aws/r/route.html
resource "aws_route" "default" {
  route_table_id            = aws_default_route_table.main.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.main.id
}

# https://www.terraform.io/docs/providers/aws/r/eip.html
resource "aws_eip" "main" {
  count = var.skip_nat ? 0 : length(var.availability_zones)
  tags = {
    Environment = var.env
    Name = "${var.env}-${var.availability_zones[count.index]}-ngw--ip"
  }
}

# https://www.terraform.io/docs/providers/aws/r/nat_gateway.html
resource "aws_nat_gateway" "main" {
  count = var.skip_nat ? 0 : length(var.availability_zones)
  allocation_id = aws_eip.main[count.index].id
  subnet_id     = aws_subnet.main[count.index].id
  tags = {
    Environment = var.env
    Name = "${var.env}-${var.availability_zones[count.index]}-ngw"
  }
}

# https://www.terraform.io/docs/providers/aws/r/route_table.html
resource "aws_route_table" "nat" {
  count = var.skip_nat ? 0 : length(var.availability_zones)
  vpc_id = aws_vpc.main.id
  tags = {
    Environment = var.env
    Name = "${var.env}-nat-${var.availability_zones[count.index]}-rtb"
  }
}

# https://www.terraform.io/docs/providers/aws/r/route.html
resource "aws_route" "nat" {
  count = var.skip_nat ? 0 : length(var.availability_zones)
  route_table_id = aws_route_table.nat[count.index].id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = aws_nat_gateway.main[count.index].id
}

# https://www.terraform.io/docs/providers/aws/r/route_table_association.html
resource "aws_route_table_association" "nat" {
  count = var.skip_nat ? 0 : length(var.availability_zones)
  subnet_id = aws_subnet.nat[count.index].id
  route_table_id = aws_route_table.nat[count.index].id
}

# https://www.terraform.io/docs/providers/aws/r/ec2_transit_gateway.html
resource "aws_ec2_transit_gateway" "main" {
  count = var.create_transit_gateway ? 1 : 0
  tags = {
    Environment = var.env
    Name = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/ec2_transit_gateway_vpc_attachment.html
resource "aws_ec2_transit_gateway_vpc_attachment" "maintg" {
  count = var.create_transit_gateway_local_attachment ? 1 : 0
  subnet_ids         = concat(aws_subnet.main.*.id)
  transit_gateway_id = aws_ec2_transit_gateway.main[count.index].id
  vpc_id             = aws_vpc.main.id
  tags = {
    Environment = var.env
    Name = var.env
  }
}


# https://www.terraform.io/docs/providers/aws/r/ec2_transit_gateway_vpc_attachment.html
resource "aws_ec2_transit_gateway_vpc_attachment" "main" {
  count = var.create_transit_gateway_attachment ? 1 : 0
  subnet_ids         = concat(aws_subnet.main.*.id)
  transit_gateway_id = var.transit_gateway_id
  vpc_id             = aws_vpc.main.id
}
