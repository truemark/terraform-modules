# TrueMark Terraform Modules

This repository contains public Terraform modules created and used by TrueMark
professional services. If you would like more information regarding our services, please visit 
https://truemark.io or reach out to sales@truemark.io.

## Branching Strategy

**master** - Always contains our latest released code.

**release/*** - Changes to any release branch are meant to be backward compatible.

All other branches are used for development purposes.

When referring to any code in this repository, we recommend you target a
specific commit or a release branch. If you refer to master you risk
automation breaking when changes are made which are not backward compatible.

## Issue Tracking

Please submit any feature requests, improvements or bugs to

https://bitbucket.org/truemark/terraform-modules/issues?status=new&status=open
