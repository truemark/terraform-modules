output "subnet_id" {
  value = azurerm_subnet.bastion.id
}

output "public_ip_address" {
  value = azurerm_public_ip.bastion.ip_address
}

output "public_ip_address_name" {
  value = azurerm_public_ip.bastion.name
}

output "bastion_host_id" {
  value = azurerm_bastion_host.bastion.id
}
