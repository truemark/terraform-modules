# Single Page Application

This module sets up all the resources needed to host a single page application 
such as applications built on Angular or React. This module will create the
following resources:

 * S3 Bucket
 * CloudFront Distribution
 * SSL Certificate
 * IAM User
 * DNS Records (optional for AWZ hosted zones)
 * Lambda Edge Function (optional for redirects) 
