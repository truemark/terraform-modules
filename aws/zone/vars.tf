variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "zone_names" {
  description = "DNS zone to create"
  type = list(string)
}
