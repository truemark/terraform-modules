provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

locals {
  env = var.env
}

# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "app" {
  name = "${var.name}-${var.env}-lb"
  description = "Controls access to ALB ${var.name}-${var.env}-lb"
  vpc_id = var.vpc_id
  tags = {
    Name = "${var.name}-${var.env}-lb"
    Environment = "main"
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
resource "aws_security_group_rule" "http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app.id
}

resource "aws_security_group_rule" "https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app.id
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.app.id
}

# https://www.terraform.io/docs/providers/aws/r/lb.html
resource "aws_alb" "app" {
  name            = "${var.name}-${var.env}-lb"
  internal        = var.internal
  subnets         = var.subnet_ids
  security_groups = [aws_security_group.app.id]
}

data "aws_acm_certificate" "app" {
  domain = var.certificate_domain
  statuses = ["ISSUED"]
}

# https://www.terraform.io/docs/providers/aws/r/lb_listener.html
resource "aws_lb_listener" "redirect" {
  load_balancer_arn = aws_alb.app.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# https://www.terraform.io/docs/providers/aws/r/lb_listener.html
resource "aws_lb_listener" "app" {
  load_balancer_arn = aws_alb.app.arn
  port = "443"
  protocol = "HTTPS"
  ssl_policy        = var.ssl_policy
  certificate_arn   = data.aws_acm_certificate.app.arn

  default_action {
    target_group_arn = var.default_target_group_arn
    type = "forward"
  }
}

# https://www.terraform.io/docs/providers/aws/d/route53_zone.html
data "aws_route53_zone" "app" {
  count = var.create_dns_record == true ? 1 : 0
  name = var.zone_name
}

# https://www.terraform.io/docs/providers/aws/r/route53_record.html
resource "aws_route53_record" "app" {
  count   = var.create_dns_record == true ? 1 : 0
  zone_id = data.aws_route53_zone.app[count.index].id
  name    = var.domain_name
  type    = "CNAME"
  ttl     = var.dns_ttl
  records = [aws_alb.app.dns_name]
}
