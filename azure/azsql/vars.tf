variable "env" {
  description = "Enviromment name"
}

variable "location" {
  description = "The location resources will be created in"
}

variable "rg_name" {
  description = "Name of the resource group"
}

variable "rg_create" {
  description = "True to create the resource group, false to just refer to it"
  type = bool
  default = true
}

variable "sql_server_name" {
  description = "Name of the SQL server to create"
}

variable "sql_server_version" {
  description = "Version of the SQL Server"
  default = "12.0"
}

variable "administrator_login" {
  description = "Username for the SQL admin"
}

variable "administrator_login_password" {
  description = "Password for the SQL admin"
}

variable "elastic_pools_dtu" {
  description = "List of elastic pools to create"
  type = list(object({
    name=string
    max_size_gb=number
    sku=object({
      name=string
      tier=string
      capacity=number
    })
    db_min_capacity=number
    db_max_capacity=number
  }))
}

variable "firewall_rules" {
  description = "List of firewall rules to add"
  type = list(object({name=string,start_ip_address=string,end_ip_address=string}))
}
