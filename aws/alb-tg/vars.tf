variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "vpc_id" {
  description = "ID of the VPC to put the target group in"
}

variable "port" {
  description = "Port application listens on. ex. 8080"
  default = 8080
}

variable "name" {
  description = "Name of the service. This will be prefixed with the environment. ex. myapp"
}

variable "health_check_interval" {
  description = "Interval in seconds health should be checked. ex. 10"
  default = 10
}

variable "health_check_path" {
  description = "URL path the health check should use. ex. /actuator/health"
}

variable "health_check_timeout" {
  description = "Time in seconds before a health check times out. ex. 5"
  default = 5
}

variable "healthy_threshold" {
  description = "Number of consecutive health checks successes required before considering an unhealthy target healthy. ex. 3"
  default = 3
}

variable "unhealthy_threshold" {
  description = "Number of consecutive health check failures required before considering the target unhealthy. ex. 3"
  default = 3
}

variable "health_check_http_codes" {
  description = "The HTTP status codes to accept for a health check. ex. 200-299"
  default = "200-299"
}

variable "stickiness_enabled" {
  default = false
}

variable "stickiness_duration" {
  description = "The time period, in seconds, during which requests from a client should be routed to the same target. ex. 86400"
  default = 86400
}
