output "security_group_id" {
  value = aws_security_group.ora.id
}

output "instance_id" {
  value = aws_instance.ora.id
}

output "swap_ebs_volume_id" {
  value = aws_ebs_volume.swap.id
}

output "swap_volume_attachment_id" {
  value = aws_volume_attachment.swap.id
}

output "software_ebs_volume_id" {
  value = aws_ebs_volume.software.id
}

output "software_volume_attachment_id" {
  value = aws_volume_attachment.software.id
}

output "data_ebs_volume_id" {
  value = aws_ebs_volume.data.id
}

output "data_volume_attachment_id" {
  value = aws_volume_attachment.data.id
}

output "fra_ebs_volume_id" {
  value = aws_ebs_volume.fra.id
}

output "fra_volume_attachment_id" {
  value = aws_volume_attachment.fra.id
}
