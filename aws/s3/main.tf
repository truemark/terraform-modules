provider "aws" {
  version = "~>2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/r/s3_bucket.html
resource "aws_s3_bucket" "s3" {
  count = length(var.buckets)
  bucket = var.buckets[count.index].name
  acl = var.buckets[count.index].acl
  force_destroy = var.buckets[count.index].force_destroy
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
 }

  versioning {
    enabled = var.buckets[count.index].versioning
  }

  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/s3_bucket_public_access_block.html
resource "aws_s3_bucket_public_access_block" "s3" {
  count = length(var.buckets)
  bucket = aws_s3_bucket.s3[count.index].id
  block_public_acls = var.buckets[count.index].block_public_acls
  block_public_policy = var.buckets[count.index].block_public_policy
  restrict_public_buckets = var.buckets[count.index].restrict_public_buckets
  ignore_public_acls = var.buckets[count.index].ignore_public_acls
}

# https://www.terraform.io/docs/providers/aws/r/iam_user.html
resource "aws_iam_user" "s3" {
  count = var.iam_user_create == true ? 1 : 0
  name = var.iam_user_name
  force_destroy = var.iam_force_destroy
  path = "/terraform/"

  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_access_key.html
resource "aws_iam_access_key" "s3" {
  count = var.iam_user_create == true ? 1 : 0
  user = var.iam_user_name
}

# https://www.terraform.io/docs/providers/template/d/file.html
data "template_file" "s3" {
  count = var.iam_user_create == true ? length(var.buckets) : 0
  template = file("user_policy.json")
  vars = {
    s3_bucket_arn = var.iam_user_create == true ? aws_s3_bucket.s3[0].arn : ""
  }
}

#https://www.terraform.io/docs/providers/aws/r/iam_user_policy.html
resource "aws_iam_user_policy" "s3" {
  count = var.iam_user_create == true ? length(var.buckets) : 0
  name = var.buckets[count.index].name
  user = var.iam_user_create == true ? aws_iam_user.s3[0].name : ""
  policy = data.template_file.s3[count.index].rendered
}
