output "vpc_id" {
  value = data.aws_vpc.main.id
}

output "vpc_arn" {
  value = data.aws_vpc.main.arn
}
