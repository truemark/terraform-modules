variable "env" {
  description = "Enviromment name"
}

variable "location" {
  description = "The location resources will be created in"
}

variable "name" {
  description = "Name of the resource group to place the resources in"
}
