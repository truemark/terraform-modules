# See https://bitbucket.org/truemark/terraform-modules/src/master/azure/bastion/
provider "azurerm" {
  version = "~>1.38.0"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "azurerm" {}
}

resource "azurerm_resource_group" "rg" {
  count = var.create_rg ? 1 : 0
  name     = var.rg_name
  location = var.location

  tags = {
    environment = var.env
  }
}

resource "azurerm_subnet" "bastion" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = var.vnet_rg_name
  virtual_network_name = var.vnet_name
  address_prefix       = var.address_prefix
}

resource "azurerm_public_ip" "bastion" {
  name                = "${var.env}-bastion"
  location            = var.location
  resource_group_name = var.rg_name
  allocation_method   = "Static"
  sku                 = "Standard"
  tags = {
    "Environment" = var.env
  }
}

resource "azurerm_bastion_host" "bastion" {
  name                = "${var.env}-bastion"
  location            = var.location
  resource_group_name = var.rg_name

  ip_configuration {
    name                 = "${var.env}-bastion"
    subnet_id            = azurerm_subnet.bastion.id
    public_ip_address_id = azurerm_public_ip.bastion.id
  }

  tags = {
    "Environment" = var.env
  }
}
