variable "region" {
  description = "The region to search for resources. ex. us-east-1"
}

variable "vpc_id" {
  description = "ID of the VPC the subnets belong to"
}

variable "tag_name" {
  description = "Name of the tag to match"
}

variable "tag_values" {
  description = "Values of the tag to match"
  type = list(string)
}


