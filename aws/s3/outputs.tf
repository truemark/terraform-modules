output "s3_bucket_ids" {
  value = aws_s3_bucket.s3.*.id
}

output "s3_bucket_arns" {
  value = aws_s3_bucket.s3.*.arn
}

output "iam_user_id" {
  value = var.iam_user_create == false ? "" : aws_iam_user.s3[0].id
}

output "iam_access_key_id" {
  value = var.iam_user_create == false ? "" : aws_iam_access_key.s3[0].id
}

output "iam_access_key_secret" {
  value = var.iam_user_create == false ? "" : aws_iam_access_key.s3[0].secret
}
