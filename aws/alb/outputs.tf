output "alb_id" {
  value = aws_alb.app.id
}

output "alb_arn" {
  value = aws_alb.app.arn
}

output "security_group_id" {
  value = aws_security_group.app.id
}

output "alb_listener_id" {
  value = aws_lb_listener.app.id
}

output "alb_dns_name" {
  value = aws_alb.app.dns_name
}

output "env" {
  value = local.env
}
