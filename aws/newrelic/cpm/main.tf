# See https://bitbucket.org/truemark/terraform-modules/src/master/aws/newrelic/cpm/
provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/d/subnet.html
data "aws_subnet" "cpm" {
  id = var.subnet_id
}

# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "cpm" {
  name = "${var.env}-${var.name}-sg"
  vpc_id = data.aws_subnet.cpm.vpc_id
  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
resource "aws_security_group_rule" "allow-ssh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.cpm.id
}

resource "aws_security_group_rule" "egress" {
  type            = "egress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks =   ["0.0.0.0/0"]
  security_group_id = aws_security_group.cpm.id
}

# https://www.terraform.io/docs/providers/aws/d/ami.html
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

# https://www.terraform.io/docs/providers/aws/r/instance.html
resource "aws_instance" "cpm" {
  ami = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.cpm.id]
  subnet_id = var.subnet_id

  root_block_device {
    volume_type = "gp2"
    volume_size = var.root_volume_size
    delete_on_termination = true
  }

  ebs_block_device {
    device_name = "/dev/xvdb"
    volume_size = 50
    volume_type = "gp2"
    delete_on_termination = true
  }

    # TODO This to be moved to a separate script or salt
  user_data = <<USER_DATA
#!/usr/bin/env bash

# Setup the default user used by pro services
groupadd user
useradd -d /home/user -g user -G sudo,admin -m -s /bin/bash user
mkdir -p /home/user/.ssh
chmod 700 /home/user/.ssh
touch /home/user/.ssh/authorized_keys
chmod 600 /home/user/.ssh/authorized_keys
chown -R user:user /home/user/.ssh

cat <<EOF > /home/user/.ssh/authorized_keys
${var.ssh_authorized_keys}
EOF

echo 'user ALL=(ALL:ALL) NOPASSWD:ALL' > /etc/sudoers.d/user

# Install TrueMark utilities
bash <(curl -sL https://bitbucket.org/truemark/linux-toolbox/raw/master/install)

# Install NewRelic Infrastructure
echo "license_key: ${var.nr_license_key}" | sudo tee -a /etc/newrelic-infra.yml
curl -sSL https://download.newrelic.com/infrastructure_agent/gpg/newrelic-infra.gpg | sudo apt-key add -
printf "deb [arch=amd64] http://download.newrelic.com/infrastructure_agent/linux/apt bionic main" | sudo tee -a /etc/apt/sources.list.d/newrelic-infra.list
apt-get -qq update
apt-get -qq install newrelic-infra

# Setup docker patition
apt-get -qq update
apt-get -qq install xfsprogs
mkfs.ext4 $(tmdisk 1)
mkdir /var/lib/docker
echo "$(blkid | grep $(tmdisk 1) | awk '{print $2}') /var/lib/docker xfs defaults 0 1" >> /etc/fstab
mount -a

# Adding docker configs
cat > /etc/sysctl.d/95-docker.conf <<EOF
fs.inotify.max_user_watches=524288
fs.inotify.max_user_instances=16384
fs.file-max=5000000
EOF

# Applying settings
sysctl -p

# Adding docker security configs
cat > /etc/security/limits.d/95-docker.conf <<EOF
* - nofile 500000
* - nproc 500000
EOF

mkfs.xfs -n ftype=1 -f /dev/vg2/docker

# Install Docker
apt-get -qq install apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get -qq update
apt-get -qq install docker-ce docker-ce-cli containerd.io

# Pull latest cpm and spin up minion
docker pull quay.io/newrelic/synthetics-minion:latest
for i in {1..3}; do docker run -e MINION_PRIVATE_LOCATION_KEY=${var.nr_minion_private_location} -v /tmp:/tmp:rw -v /var/run/docker.sock:/var/run/docker.sock:rw --detach quay.io/newrelic/synthetics-minion:latest; done
docker container ls -a
df -h

USER_DATA

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}
