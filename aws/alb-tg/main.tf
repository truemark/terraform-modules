provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/r/lb_target_group.html
resource "aws_lb_target_group" "app" {
  name     = "${var.name}-${var.env}-tg"
  port     = var.port
  protocol = "HTTP"
  vpc_id   = var.vpc_id
  target_type = "ip"

  health_check {
    enabled = true
    interval = var.health_check_interval
    path = var.health_check_path
    timeout = var.health_check_timeout
    healthy_threshold = var.healthy_threshold
    unhealthy_threshold = var.unhealthy_threshold
    matcher = var.health_check_http_codes
  }

  stickiness {
    type = "lb_cookie"
    enabled = var.stickiness_enabled
    cookie_duration = var.stickiness_duration
  }
}
