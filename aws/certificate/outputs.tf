output "acm_certificate_id" {
  value = aws_acm_certificate.crt.id
}

output "acm_certificate_domain_name" {
  value = aws_acm_certificate.crt.domain_name
}

output "route53_validation_record_id" {
  value = aws_route53_record.validation.id
}

output "acm_certificate_validation_id" {
  value = aws_acm_certificate_validation.crt
}
