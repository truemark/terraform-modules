variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "name" {
  description = "Name of the RDS instance"
}

variable "vpc_id" {
  description = "ID of the VPC to use"
}
