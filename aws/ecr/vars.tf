variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "repositories" {
  description = "A list of repositories"
  type = list(map(any))
  default = [
    {
      name = "helloworld"
      image_tag_mutability = "MUTABLE"
      scan_on_push = false
    }
  ]
}
