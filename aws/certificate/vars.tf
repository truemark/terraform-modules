variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "zone_name" {
  description = "Zone the domain name will be hosted under"
}

variable "domain_name" {
  description = "Domain prefix the site will be hosted under"
}
