variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "cidr_block" {
  description = "The CIDR block for the VPC. ex. 10.25.0.0/16"
}

variable "newbits" {
  description = "Number of additional bits with which to extend the cidr block prefix"
  default = 8
}

variable "availability_zones" {
  description = "The list of availability zones uses in this VPC"
  type = list(string)
  default = ["a", "b", "c"]
}

variable "nat_newbits" {
  description = "Number of additional bits with which to extend the cidr block prefix for nat subnets"
  default = 4
}

variable "nat_offset" {
  description = "Network offset for nat subnets"
  default = 1
}

variable "skip_nat" {
  description = "Set to true to skip nat subnet creation"
  type = bool
  default = false
}

variable "create_transit_gateway" {
  description = "True to create transit gateway"
  type = bool
  default = false
}

variable "create_transit_gateway_local_attachment" {
  description = "True to create transit gateway attachments for the local subnets"
  type = bool
  default = false
}

variable "create_transit_gateway_attachment" {
  description = "Set to false to not skip transit gateway attachment"
  type = bool
  default = false
}

variable "transit_gateway_id" {
  default = ""
}
