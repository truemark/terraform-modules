provider "azurerm" {
  version = "~>1.38.0"
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "azurerm" {}
}

resource "azurerm_resource_group" "rg" {
  name     = var.name
  location = var.location

  tags = {
    environment = var.env
  }
}
