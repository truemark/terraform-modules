provider "aws" {
  version = "=2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

# https://www.terraform.io/docs/providers/aws/d/subnet.html
data "aws_subnet" "ora" {
  id = var.subnet_id
}

# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "ora" {
  name = "${var.env}-${var.name}-sg"
  vpc_id = data.aws_subnet.ora.vpc_id
  tags = {
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
resource "aws_security_group_rule" "allow-ssh" {
  type            = "ingress"
  from_port       = 22
  to_port         = 22
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.ora.id
}

resource "aws_security_group_rule" "allow-oracle" {
  type            = "ingress"
  from_port       = 1521
  to_port         = 1521
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.ora.id
}

resource "aws_security_group_rule" "allow-actifio-1" {
  type            = "ingress"
  from_port       = 5106
  to_port         = 5106
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.ora.id
}

resource "aws_security_group_rule" "allow-actifio-2" {
  type            = "ingress"
  from_port       = 56789
  to_port         = 56789
  protocol        = "tcp"
  cidr_blocks     = var.allowed_cidr_blocks
  security_group_id = aws_security_group.ora.id
}

resource "aws_security_group_rule" "egress" {
  type            = "egress"
  from_port       = 0
  to_port         = 0
  protocol        = "-1"
  cidr_blocks =   ["0.0.0.0/0"]
  security_group_id = aws_security_group.ora.id
}

# https://www.terraform.io/docs/providers/aws/d/ami.html
data "aws_ami" "ol" {
  most_recent = true

  filter {
    name   = "name"
    values = ["OL7.6-x86_64-HVM-2019-01-29"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["131827586825"]
}

# https://www.terraform.io/docs/providers/aws/r/instance.html
resource "aws_instance" "ora" {
  ami = data.aws_ami.ol.id
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = [aws_security_group.ora.id]
  subnet_id = var.subnet_id

  root_block_device {
    volume_type = "gp2"
    volume_size = var.root_volume_size
    delete_on_termination = true
  }

  # TODO This to be moved to a separate script or salt
  user_data = <<USER_DATA
#!/usr/bin/env bash

# Setup the default user used by pro services
groupadd user
useradd -d /home/user -g user -G wheel -m -s /bin/bash user
mkdir -p /home/user/.ssh
chmod 700 /home/user/.ssh
touch /home/user/.ssh/authorized_keys
chmod 600 /home/user/.ssh/authorized_keys
chown -R user:user /home/user/.ssh

cat <<EOF > /home/user/.ssh/authorized_keys
${var.ssh_authorized_keys}
EOF

echo 'user ALL=(ALL:ALL) NOPASSWD:ALL' > /etc/sudoers.d/user

# Install dependencies
yum install -y nvme-cli vim unzip \
  oracleasm-support \
  oracle-rdbms-server-11gR2-preinstall \
  oracle-rdbms-server-12cR1-preinstall \
  oracle-database-server-12cR2-preinstall \
  oracle-database-preinstall-18c \
  oracle-database-preinstall-19c

# Install TrueMark utilities
bash <(curl -sL https://bitbucket.org/truemark/linux-toolbox/raw/master/install)

# Partition and setup

mkswap $(tmdisk 1)
echo "$(blkid | grep $(tmdisk 1) | awk '{print $2}') swap swap defaults 0 0" >> /etc/fstab
swapon $(tmdisk 1)

mkfs.ext4 $(tmdisk 2)
mkdir /u01
echo "$(blkid | grep $(tmdisk 2) | awk '{print $2}') /u01 ext4 defaults 0 1" >> /etc/fstab
mount -a

# DATA
parted --script $(tmdisk 3) mklabel gpt mkpart primary 0% 100%
# FRA
parted --script $(tmdisk 4) mklabel gpt mkpart primary 0% 100%

oracleasm configure -u oracle -g oinstall -e -s y
oracleasm init
oracleasm createdisk DATA1 $(tmdisk 3)p1
oracleasm createdisk FRA $(tmdisk 4)p1

systemctl stop firewalld
systemctl disable firewalld

cat <<EOF > /root/prep.sh
${var.prep_script}
EOF
chmod +x /root/prep.sh
cd /root && ./prep.sh

USER_DATA

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}

resource "aws_ebs_volume" "swap" {
  availability_zone = data.aws_subnet.ora.availability_zone
  type = "gp2"
  size = var.swap_volume_size

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}

resource "aws_volume_attachment" "swap" {
  device_name = "/dev/xvdb"
  instance_id = aws_instance.ora.id
  volume_id = aws_ebs_volume.swap.id
}

resource "aws_ebs_volume" "software" {
  availability_zone = data.aws_subnet.ora.availability_zone
  type = "gp2"
  size = var.software_volume_size

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}

resource "aws_volume_attachment" "software" {
  device_name = "/dev/xvdc"
  instance_id = aws_instance.ora.id
  volume_id = aws_ebs_volume.software.id
}

resource "aws_ebs_volume" "data" {
  availability_zone = data.aws_subnet.ora.availability_zone
  type = "gp2"
  size = var.data_volume_size

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}

resource "aws_volume_attachment" "data" {
  device_name = "/dev/xvdd"
  instance_id = aws_instance.ora.id
  volume_id = aws_ebs_volume.data.id
}

resource "aws_ebs_volume" "fra" {
  availability_zone = data.aws_subnet.ora.availability_zone
  type = "gp2"
  size = var.fra_volume_size

  tags = {
    Environment = var.env
    Name = "${var.env}-${var.name}"
  }
}

resource "aws_volume_attachment" "fra" {
  device_name = "/dev/xvde"
  instance_id = aws_instance.ora.id
  volume_id = aws_ebs_volume.fra.id
}
