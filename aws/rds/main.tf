provider "aws" {
  version = "~>2.0"
  region  = var.region
}

resource "aws_security_group" "rds" {
  name = "${var.name}-rds"
  description = "allows access to rds instance"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.name}-rds"
  }
}


