output "cloudwatch_log_group_name" {
  value = aws_cloudwatch_log_group.ecs.name
}

output "cloudwatch_log_group_arn" {
  value = aws_cloudwatch_log_group.ecs.arn
}

output "cloudwatch_log_group_id" {
  value = aws_cloudwatch_log_group.ecs.id
}

output "ecs_security_group_name" {
  value = aws_security_group.ecs_tasks.name
}

output "ecs_security_group_arn" {
  value = aws_security_group.ecs_tasks.arn
}

output "ecs_security_group_id" {
  value = aws_security_group.ecs_tasks.id
}

output "ecs_iam_role_name" {
  value = aws_iam_role.ecs.name
}

output "ecs_iam_role_id" {
  value = aws_iam_role.ecs.id
}

output "ecs_iam_role_arn" {
  value = aws_iam_role.ecs.arn
}

output "ecs_iam_role_policy_name" {
  value = aws_iam_role_policy.ecs.name
}

output "ecs_iam_role_policy_id" {
  value = aws_iam_role_policy.ecs.id
}

output "ecs_task_definition_id" {
  value = aws_ecs_task_definition.service.id
}

output "ecs_task_definition_arn" {
  value = aws_ecs_task_definition.service.arn
}

output "ecs_service_id" {
  value = aws_ecs_task_definition.service.id
}

output "ecs_service_arn" {
  value = aws_ecs_task_definition.service.arn
}

output "appautoscaling_target_id" {
  value = aws_appautoscaling_target.target.id
}

output "up_appautoscaling_policy_id" {
  value = aws_appautoscaling_policy.up.id
}

output "up_appautoscaling_policy_arn" {
  value = aws_appautoscaling_policy.up.arn
}

output "down_appautoscaling_policy_id" {
  value = aws_appautoscaling_policy.down.id
}

output "down_appautoscaling_policy_arn" {
  value = aws_appautoscaling_policy.down.arn
}

output "high_cpu_cloudwatch_metric_alarm_name" {
  value = aws_cloudwatch_metric_alarm.service_cpu_high.alarm_name
}

output "high_cpu_cloudwatch_metric_alarm_id" {
  value = aws_cloudwatch_metric_alarm.service_cpu_high.id
}

output "high_cpu_cloudwatch_metric_alarm_arn" {
  value = aws_cloudwatch_metric_alarm.service_cpu_high.arn
}

output "low_cpu_cloudwatch_metric_alarm_name" {
  value = aws_cloudwatch_metric_alarm.service_cpu_low.alarm_name
}

output "low_cpu_cloudwatch_metric_alarm_id" {
  value = aws_cloudwatch_metric_alarm.service_cpu_low.id
}

output "low_cpu_cloudwatch_metric_alarm_arn" {
  value = aws_cloudwatch_metric_alarm.service_cpu_low.arn
}
