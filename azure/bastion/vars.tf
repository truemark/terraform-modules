variable "env" {
  description = "Enviromment name"
}

variable "location" {
  description = "The location resources will be created in"
}

variable "create_rg" {
  description = "True to create the resource group, false to just refer to it"
  default = true
}

variable "rg_name" {
  description = "Name of the resource group to place the resources in"
}

variable "vnet_name" {
  description = "The virtual network name"
}

variable "vnet_rg_name" {
  description = "Name of the resource group the vnet is in"
}

variable "address_prefix" {
  description = "The address space to use for the subnet"
}
