variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
  default = "us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "s3_bucket_prefix" {
  description = "A prefix to be used on the s3 bucket name. You can use the account ID or the company name for example."
}

variable "s3_force_destroy" {
  description = "By default S3 buckets with content will not be destroy by terraform when destroying a stack"
  default = false
}

variable "name" {
  description = "The application name being deployed. ex. website"
}

variable "zone_name" {
  description = "Zone the site will be hosted in. Leave null to not create DNS records."
  default = null
}

variable "domain_name" {
  description = "Domain the site will be hosted under."
}

variable "certificate_domain" {
  description = "The name of the certificate to use. Leave blank to generate a certificate."
  default = null
}

variable "subject_alternative_name_zone_names" {
  description = "Route 53 zones for each of the subject alternative names"
  type = list(string)
  default = []
}

variable "subject_alternative_names" {
  description = "Subject alternative names the site will respond to"
  type = list(string)
  default = []
}

variable "enable_redirect" {
  description = "Enables redirecting alternative names to the domain name using Lambda Edge"
  default = true
}

# See https://aws.amazon.com/cloudfront/pricing/
variable "price_class" {
  description = "Price class for the CloudFront Distribution"
  default = "PriceClass_All"
}

# See https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html#DownloadDistValues-security-policy
variable "minimum_protocol_version" {
  description = "Security policy to apply to the CloudFront distribution"
  default = "TLSv1.2_2018"
}

variable "skip_local_file" {
  description = "Used to exclude local_file from plan-all"
  default = false
}
