variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "buckets" {
  description = "A list of buckets"
  type = list(object(
    {
      name=string
      acl=string
      versioning=bool
      force_destroy=bool
      block_public_acls=bool
      block_public_policy=bool
      restrict_public_buckets=bool
      ignore_public_acls=bool
    }))
  default = [
    {
      name = "terraform-example"
      acl = "private"
      versioning = false
      force_destroy = false
      block_public_acls = true
      block_public_policy = true
      restrict_public_buckets = true
      ignore_public_acls = true
    }
  ]
}

variable "iam_user_create" {
  description = "True to create an IAM user with access to the buckets"
  type = bool
  default = true
}

variable "iam_user_name" {
  description = "Name od the IAM user"
  type = string
  default = ""
}

variable "iam_force_destroy" {
  description = "True to force destroy the IAM user"
  type = bool
  default = true
}
