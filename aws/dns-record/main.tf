# See https://bitbucket.org/truemark/terraform-modules/src/master/aws/zone/
provider "aws" {
  version = "~>2.42.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

data "aws_route53_zone" "dns" {
  name = var.zone_name
  private_zone = var.zone_private
}

resource "aws_route53_record" "dns" {
  count = length(var.records)
  zone_id = data.aws_route53_zone.dns.zone_id
  name = var.records[count.index].name
  type = var.records[count.index].type
  ttl = var.records[count.index].ttl
  records = var.records[count.index].values
}
