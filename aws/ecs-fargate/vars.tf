variable "region" {
  description = "The region to spin resources up in. ex. us-east-1"
}

variable "env" {
  description = "The environment the resources belong to. ex. test"
}

variable "name" {
  description = "The name of the cluster. ex myapps"
}

variable "suffix" {
  description = "The suffix to use on the name of the cluster. Default is -ecs"
  default = "-ecs"
}
