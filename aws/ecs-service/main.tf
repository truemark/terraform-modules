provider "aws" {
  version = "~> 2.0"
  region  = var.region
}

terraform {
  # The configuration for this backend will be filled in by Terragrunt
  backend "s3" {}
}

resource "aws_cloudwatch_log_group" "ecs" {
  name = "${var.name}-${var.env}"
  retention_in_days = 30

  tags = {
    Name = "${var.name}-${var.env}"
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group.html
resource "aws_security_group" "ecs_tasks" {
  name        = "${var.name}-${var.env}-ecs"
  description = "allow inbound access from the ALB only"
  vpc_id      = var.vpc_id
  tags = {
    Name = "${var.name}-${var.env}-ecs"
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/security_group_rule.html
resource "aws_security_group_rule" "alb" {
  type              = "ingress"
  from_port         = var.port
  to_port           = var.port
  protocol          = "tcp"
  security_group_id = aws_security_group.ecs_tasks.id
  source_security_group_id = var.alb_security_group_id
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ecs_tasks.id
}

# https://www.terraform.io/docs/providers/aws/r/iam_role.html
resource "aws_iam_role" "ecs" {
  name = "${var.name}-${var.env}-ecs-service"
  assume_role_policy = file("assume_role_policy.json")
  tags = {
    Name = "${var.name}-${var.env}-ecs-service"
    Environment = var.env
  }
}

# https://www.terraform.io/docs/providers/aws/r/iam_role_policy.html
resource "aws_iam_role_policy" "ecs" {
  name = "${var.name}-${var.env}-ecs"
  role = aws_iam_role.ecs.id
  policy = templatefile("role_policy.json", var.role_policy_vars)
}


# https://www.terraform.io/docs/providers/template/d/file.html
data "template_file" "task_definition" {
  # https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_ContainerDefinition.html
  template = file("task_definition.json")
  vars = {
    name      = var.name
    image     = var.image
    port      = var.port
    cpu       = var.cpu
    memory    = var.memory
    region    = var.region
    log_group = aws_cloudwatch_log_group.ecs.name
    log_stream_prefix = "${var.name}-${var.env}"
    environment = jsonencode(var.environment)
    credentials = var.docker_credentials_arn == null ? "" : "\"repositoryCredentials\": {\"credentialsParameter\": \"${var.docker_credentials_arn}\"},"
  }
}

# https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html
resource "aws_ecs_task_definition" "service" {
  container_definitions = data.template_file.task_definition.rendered
  family = "${var.name}-${var.env}-task"
  execution_role_arn = aws_iam_role.ecs.arn
  network_mode = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu = var.cpu
  memory = var.memory
}

# https://www.terraform.io/docs/providers/aws/r/ecs_service.html
resource "aws_ecs_service" "service" {
  name = "${var.name}-${var.env}-service"
  cluster = var.ecs_cluster_id
  task_definition = aws_ecs_task_definition.service.arn
  desired_count = var.desired_count
  launch_type = "FARGATE"

  network_configuration {
    security_groups  = [aws_security_group.ecs_tasks.id]
    subnets          = var.subnet_ids
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = var.alb_target_group_arn
    container_name   = var.name
    container_port   = var.port
  }

  depends_on = [aws_iam_role_policy.ecs]

  # Allow external changes to desired_count
  lifecycle {
    ignore_changes = ["desired_count"]
  }
}

# https://www.terraform.io/docs/providers/aws/r/appautoscaling_target.html
resource "aws_appautoscaling_target" "target" {
  service_namespace  = "ecs"
  resource_id        = "service/${var.ecs_cluster_name}/${var.name}-${var.env}-service"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity       = var.desired_count
  max_capacity       = var.max_capacity

  depends_on = [aws_ecs_service.service]
}

# Automatically scale capacity up by one
resource "aws_appautoscaling_policy" "up" {
  name               = "${var.env}_${var.name}-up"
  service_namespace  = "ecs"
  resource_id        = "service/${var.ecs_cluster_name}/${var.name}-${var.env}-service"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = [aws_appautoscaling_target.target]
}

# Automatically scale capacity down by one
resource "aws_appautoscaling_policy" "down" {
  name               = "${var.env}_${var.name}-down"
  service_namespace  = "ecs"
  resource_id        = "service/${var.ecs_cluster_name}/${var.name}-${var.env}-service"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 60
    metric_aggregation_type = "Maximum"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = -1
    }
  }

  depends_on = [aws_appautoscaling_target.target]
}

# CloudWatch alarm that triggers the autoscaling up policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_high" {
  alarm_name          = "${var.env}_${var.name}_cpu_utilization_high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "85"

  dimensions = {
    ClusterName = var.ecs_cluster_name
    ServiceName = "${var.name}-${var.env}-service"
  }

  alarm_actions = [aws_appautoscaling_policy.up.arn]
}

# CloudWatch alarm that triggers the autoscaling down policy
resource "aws_cloudwatch_metric_alarm" "service_cpu_low" {
  alarm_name          = "${var.env}_${var.name}_cpu_utilization_low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = "10"

  dimensions = {
    ClusterName = var.ecs_cluster_name
    ServiceName = "${var.name}-${var.env}-service"
  }

  alarm_actions = [aws_appautoscaling_policy.down.arn]
}
